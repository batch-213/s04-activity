<?php

class Building{
	protected $name;
	protected $floors;
	protected $address; 

	public function __construct($name, $floors, $address){
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}

	public function getName(){
		return $this->name;
	}

	public function setName($name){
		$this->name = $name;
		return $this;
	}

	public function getFloors(){
		return $this->floors;
	}

	public function getAddress(){
		return $this->address;
	}
}

class Condominium extends Building{

	public function getName(){
		return $this->name;
	}

	public function setName($name){
		$this->name = $name;
		return $this;
	}

	public function getFloors(){
		return $this->floors;
	}

	public function getAddress(){
		return $this->address;
	}
}

$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon city, Philippines');

$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati city, Philippines');
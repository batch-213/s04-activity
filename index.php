<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>S4: Activity</title>
</head>
<body>
	<h1>Building</h1>

	<p>The name of the building is <?php echo $building->getName().'.'; ?></p>
	<p>The <?php echo $building->getName() .' has ' . $building->getFloors() .' floors.'; ?></p>
	<p>The <?php echo $building->getName() .' is located at ' . $building->getAddress().'.'; ?></p>
	<p>The name of the building has been change to <?php echo $building->setName('Caswynn Complex')->getName().'.'; ?></p>

	<h1>Condominium</h1>

	<p>The name of the condominium is <?php echo $condominium->getName().'.'; ?></p>
	<p>The <?php echo $condominium->getName() .' has ' . $condominium->getFloors() .' floors.'; ?></p>
	<p>The <?php echo $condominium->getName() .' is located at ' . $condominium->getAddress().'.'; ?></p>
	<p>The name of the condominium has been change to <?php echo $condominium->setName('Enzo Tower')->getName().'.'; ?></p>

</body>
</html>